# Changes
# Breaking changes
## Migration process
# New features
# Hotfix
# Fixes

<!--- TECHNICAL NOTES --->

---
# Technical Notes
This section will not be visible in release notes.

## Author checklist
Please make sure that all steps are performed before submitting to peer review.

- [ ] Make sure that all changes are reflected in project documentation
- [ ] Fill correct sections above to explain what and why is being made. 
	- [ ] At least one of five sections is required. 
	- [ ] Empty ones should be removed.

### Sections
- **Changes** - changes that do not introduce new functionalities and do not break the app (ex. do not require any additional actions from end-users to make it work)
- **Breaking changes** - changes that break the app and need additional user action to make it work, ex. configuring a variable, changing way of execution, adding parameter, etc.
  - **Migration process** - **IMPORTANT!** detailed instructions on how to implement changes required to use the new app version. **This is required, if there are any Breaking Changes.**
- **New features** - changes that introduce new features to the project. If the new feature has breaking changes, these should be included in a separate **Breaking changes** section
- **Hotfix** - bug fix that was implemented as hotfix (did not have to pass full pipeline, just the `hotfix` version of it)
- **Fixes** - changes that fix discovered or reported bugs

## Reviewer checklist
Please make sure that all steps are performed before approving the Merge Request.
- [ ] **Check author checklist.** If any steps are checked, but not performed - uncheck them and notify the author.
- [ ] **Check if tests are updated** (are code updates reflected in tests and covered by them?):
    - [ ] Unit Tests
    - [ ] Unit Integration Tests
    - [ ] Infrastructure Tests
    - [ ] E2E Tests
- [ ] If there are Breaking Changes, make sure that Migration Process is well-documented and covers all use cases.
- [ ] If there are New Features, make sure that they are well-documented for the project teams to use (ex. include example uses or detailed usage description). Will somebody from outside Shared Infra be able to use them based only on these instructions?
